variable "name" {
  description = "Name of efs resource"
  type        = "string"
}

variable "vpc_id" {
  description = "EFS is created in this VPC"
  type        = "string"
}

variable "subnets" {
  description = "A list of subnets to associate with."
  type        = "list"
}

variable "encrypted" {
  description = "Encrypt file system."
  default     = true
}

variable "performance_mode" {
  description = "maxIO"
  default     = "generalPurpose"
}

variable "creation_token" {
  type    = "string"
  default = ""
}

variable "tags" {
  type        = "map"
  description = "A map of tags to add to the resources"
  default     = {}
}

variable "access_sg_ids" {
  description = "A list of security groups Ids to grant access."
  type        = "list"
  default     = []
}
